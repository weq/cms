<?php
/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/

define('ROOT',__DIR__);

require __DIR__ . '/source/bootstrap/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

/** Loads the WordPress Environment and Template */
define('WP_ONLY', defined('ABSPATH'));
if (WP_ONLY == false) {
    require_once(__DIR__. '/home/wp-blog-header.php');
}


$app = require_once __DIR__ . '/source/bootstrap/app.php';



/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can simply call the run method,
| which will execute the request and send the response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make('Illuminate\Contracts\Http\Kernel');


$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);


is_object($response) and WP_ONLY == false and $response->send();

try {
    $kernel->terminate($request, $response);
} catch (Exception $e){
    return;
}
