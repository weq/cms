<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

//Route::get('home', 'HomeController@index');


foreach (Modules::all() as $name => $module) {
	if (Modules::find($name)->json()->type == 'theme') {
		register_theme_directory(dirname($module->getpath()));
		//kd($module->getpath());
	}
}

if(function_exists('get_template_directory')){
	if (is_dir(get_template_directory() . '/Resources/views')) {
		View::addLocation(get_template_directory() . '/Resources/views');
		View::addNamespace('tpl', get_template_directory() . '/Resources/views');
	}


	View::addLocation(get_template_directory());
	View::addNamespace('default', get_template_directory());
}



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['prefix' => 'test'], function () {
	Route::any('{all?}/{sub?}', function(){
		$path = implode('/',func_get_args());
		//kd(file_exists(ROOT . "/test/$path.php"),$path);
		if(file_exists(ROOT."/test/$path.php")){
			require ROOT . "/test/$path.php";
		}
	})->where('all', '.+');
});


Route::group(['prefix' => ''], function () {
	Route::get('/', 'WPController@index');
	Route::any('{all?}', 'WPController@index')->where('all', '.+');
});
