/*
Theme Name: Lindeza
Author: ThemesTune
Theme URI: http://www.themestune.com/theme/lindeza-free
Author URI: http://www.themestune.com
Description: Lindeza Theme is the perfect solution for creativity and business, we offer a clean, modern and responsive design for all browsers and gadgets. By developing this theme we used the latest techniques of css and html. Lindeza Theme is providing a complex design that distinguish it from the rest of websites, but also a very easy admin panel that will help you customize it by your needs. The Lindeza Theme structure allows it  to be used for any business areas.
Version: 1.0.3
Tags: green, black, white, light, one-column, two-columns, right-sidebar, responsive-layout, custom-menu, custom-background, editor-style, featured-images, full-width-template, theme-options, threaded-comments, translation-ready
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Text Domain:  lindeza
*/

== Copyright ==
Lindeza WordPress Theme, Copyright (C) 2015 Themes Tune
Lindeza WordPress Theme is licensed under the GPL 3.

Lindeza is built with the following resources: 

Plugin kirki - https://wordpress.org/plugins/kirki/
License: GPL
Copyright: Aristeides Stathopoulos, http://aristeides.com

Owl Carousel - https://github.com/OwlFonk/OwlCarousel
License: MIT 
Copyright: owlgraphic, http://owlgraphic.com/owlcarousel/

Fontello - https://github.com/fontello/fontello
License: MIT License
Copyright: Vitaly Puzrin, https://github.com/fontello/fontello/blob/master/LICENSE

Montserrat - http://www.fontsquirrel.com/fonts/montserrat
License: SIL Open Font License v1.10
Copyright: Julieta Ulanovsky, http://www.fontsquirrel.com/fonts/montserrat

Pacifico - http://www.fontsquirrel.com/fonts/pacifico
License: SIL Open Font License v1.10
Copyright: Vernon Adams, www.newtypography.co.uk

Icons (pattern.png, logo.php) - http://www.themestune.com/
License: General Public License (GPL)
Copyright: Themes Tune, http://www.themestune.com/

Main JS  - http://www.themestune.com/
License: General Public License (GPL)
Copyright: Themes Tune, http://www.themestune.com/

== Installation ==

1. Upload the `lindeza` folder to the `/wp-content/themes/` directory
Activation and Use
1. Activate the Theme through the 'Themes' menu in WordPress
2. See Appearance -> Theme Options to change theme options
