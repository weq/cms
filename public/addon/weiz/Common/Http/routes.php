<?php

Route::group(['prefix' => 'common', 'namespace' => 'Modules\Common\Http\Controllers'], function()
{
	Route::get('/', 'CommonController@index');
});