<?php namespace Modules\Common\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class CommonController extends Controller {
	
	public function index()
	{
		return view('common::index');
	}
	
}