<?php namespace Modules;

/**
 * Created by PhpStorm.
 * User: Mo
 * Date: 15-7-24
 * Time: 下午8:13
 */

class Application extends \Illuminate\Foundation\Application
{
    /**
     * Get the path to the storage directory.
     *
     * @return string
     */
    public function storagePath()
    {
        return  dirname($this->basePath) . '/data/storage';
    }
}